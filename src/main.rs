use std::collections::hash_map::HashMap;
use std::{io, fs};
use std::io::Write;
use std::path::Path;

use rayon::prelude::*;
use walkdir::{WalkDir, DirEntry};

type StatisticData = HashMap<String, u64>;

fn merge_results(
    mut acc: StatisticData,
    new: StatisticData
) -> StatisticData {
    for (k, v) in new {
        match acc.get_mut(&k) {
            Some(ov) => {
                *ov += v;
            },
            None => {
                acc.insert(k, v);
            }
        }
    }
    acc
}

fn count_file_into_map(
    entry: DirEntry,
    map: &mut StatisticData
) -> () {
    // Whitespace and trivial punctuations
    let pattern = [' ', '\n', '\r', ',', '.', '?', '!', '"'];
    let path = entry.path();
    let bytes = fs::read(path).unwrap();
    let string = String::from_utf8_lossy(&bytes);
    let iter = string.split(&pattern[..]).filter(|s| !s.is_empty());
    for word in iter {
        match map.get_mut(word) {
            Some(val) => { *val += 1; },
            None => { map.insert(word.to_string(), 1); },
        };
    }
}

fn report_into_file(
    result: StatisticData,
    dest: &Path
) -> io::Result<()> {
    let mut f = fs::File::create(dest)?;
    f.write(b"Word count result:\n")?;
    for (k, v) in result {
        f.write(format!("{}: {}\n", k, v).as_bytes())?;
    }
    Ok(())
}

fn main() {
    let args = std::env::args().collect::<Vec<_>>();
    if args.len() != 3 {
        eprintln!("Usage: wordcount [DIR] [OUTPUT]");
        std::process::exit(2);
    } else {
        let dir = &args[1];
        let output = &args[2];
        let iter = WalkDir::new(dir)
            .follow_links(true)
            .into_iter()
            .par_bridge();
        let final_res = iter
            .map(|x| x.unwrap())
            .filter(|x| x.file_type().is_file())
            .fold_with(HashMap::new(), |mut map, entry| {
                count_file_into_map(entry, &mut map);
                map
            })
            .reduce(|| HashMap::new(), merge_results);
        report_into_file(final_res, output.as_ref()).unwrap()
    }
}
